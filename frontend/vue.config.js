module.exports = {
  pages: {
    index: {
      entry: 'src/main.js',
      title: 'Vue Tailwind App'
    }
  },
  devServer: {
    disableHostCheck: true
  }
}
